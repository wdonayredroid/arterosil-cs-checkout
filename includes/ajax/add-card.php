<?php

use ZendeskCSWooCart\ArterosilTool;
use ZendeskCSWooCart\Utils;

add_action( 'wp_ajax_addCard', 'ajax_addCard_handler' );
add_action( 'wp_ajax_nopriv_' . 'addCard', 'ajax_addCard_handler' );

function ajax_addCard_handler() {
    
    //set header to return json
    header('Content-Type: application/json');

    $params = [
        'token' => $_POST['token'] ? $_POST['token'] : null,
        'type' => $_POST['type'] ? $_POST['type'] : null,
        'brand' => $_POST['brand'] ? $_POST['brand'] : null,
        'country' => $_POST['country'] ? $_POST['country'] : null,
        'exp_month' => $_POST['exp_month'] ? $_POST['exp_month'] : null,
        'exp_year' => $_POST['exp_year'] ? $_POST['exp_year'] : null,
        'last4' => $_POST['last4'] ? $_POST['last4'] : null,
        'card_id' => $_POST['card_id'] ? $_POST['card_id'] : null,
        'object' => $_POST['object'] ? $_POST['object'] : null,
        'created' => $_POST['created'] ? $_POST['created'] : null,
        'user_id' => $_POST['userID'] ? $_POST['userID'] : null,
    ];

    $aTool = new ArterosilTool(['user_id'=>$params['user_id']]);
    try{
        $ret = $aTool->addCard($params);
    }
    catch (\Exception $e){
        $ret = [ 'message' => $e->getMessage() ];
        wp_send_json_error($ret,400);
    }
    
    echo json_encode($ret);
    

    wp_die();
}