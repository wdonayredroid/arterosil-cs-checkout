<?php 

use ZendeskCSWooCart\Models\User;

add_action( 'wp_ajax_updateUserMeta', 'ajax_updateUserMeta_handler' );
add_action( 'wp_ajax_nopriv_' . 'updateUserMeta', 'ajax_updateUserMeta_handler' );

function ajax_updateUserMeta_handler() {
    $ret = null;
    
    $userID     = isset($_POST['userID'])?$_POST['userID']:null;
    $key        = isset($_POST['key'])?$_POST['key']:null;
    $value      = isset($_POST['value'])?$_POST['value']:null;

    header('Content-Type: application/json');

    try{
        $user = new User([ 'user_id'=>$userID, 'force_local'=>true ]);
        $res = (object)$user->updateMetaData([ 'key'=>$key, 'value'=>$value ]);
        if($res->success){
            wp_send_json_success('User meta data update successful!');   
        }

    } catch(Exception $e){
        wp_send_json_error($e->getMessage(), 400);
    }

    exit();
}