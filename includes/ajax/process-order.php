<?php

use ZendeskCSWooCart\ArterosilTool;

use ZendeskCSWooCart\Models\Customer;
use ZendeskCSWooCart\Models\Agent;
use ZendeskCSWooCart\Models\Order;
use ZendeskCSWooCart\Utils;

add_action( 'wp_ajax_processOrder', 'ajax_processOrder_handler' );
add_action( 'wp_ajax_nopriv_' . 'processOrder', 'ajax_processOrder_handler' );

function ajax_processOrder_handler() {
    
    //POSTS
    $param = [
        'userID' => $_POST['userID'] ? $_POST['userID'] : null,
        'line_items' => $_POST['line_items'] ? $_POST['line_items'] : null,
        'billing' => $_POST['billing'] ? $_POST['billing'] : null,
        'shipping' => $_POST['shipping'] ? $_POST['shipping'] : null,
        'card' => $_POST['card'] ? $_POST['card'] : null
    ];
    //$param = json_decode( json_encode($param) );

    //set header to return json
    header('Content-Type: application/json');

    /** MAKE ORDER */
    $tool = new ArterosilTool( ['user_id'=>$param['userID']] );

    $order = $tool->createOrder([
        'billing'       => $param['billing'],
        'shipping'      => $param['shipping'],
        'line_items'    => $param['line_items'] 
    ]);

    Utils::_()->log(json_encode($order));

    if($order){
        $processed =  ($tool->processPayment([ 'card'=>$param['card'], 'orderObj'=>$order]));
        Utils::_()->log(json_encode($processed));
        return $processed;
    }

    wp_die();
}