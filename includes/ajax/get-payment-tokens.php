<?php

use ZendeskCSWooCart\Models\PaymentTokens;

add_action( 'wp_ajax_getPaymentTokens', 'ajax_getPaymentTokens_handler' );
add_action( 'wp_ajax_nopriv_' . 'getPaymentTokens', 'ajax_getPaymentTokens_handler' );

function ajax_getPaymentTokens_handler() {
    
    //set header to return json
    header('Content-Type: application/json');

    $userID = isset($_POST['userID'])?$_POST['userID']:null;

    if(!isset($userID)) throw new \Exception('Customer id required!');

    $wc = WC_Payment_Tokens::get_customer_tokens($userID);

    $ret = [];

    foreach($wc as $key => $value){
        $cc = new WC_Payment_Token_CC($key);
        // var_dump($cc->get_data());
        $meta = $cc->get_meta_data();
        if($cc->get_data()['gateway_id'] === 'stripe'){
            $ret[$key] = [
                'token'         => $cc->get_token(),
                'last4'         => $cc->get_meta('last4'),
                'expiry_year'   => $cc->get_meta('expiry_year'),
                'expiry_month'  => $cc->get_meta('expiry_month'),
                'card_type'     => $cc->get_meta('card_type'),
                'gateway_id'    => $cc->get_data()['gateway_id']
            ];
        }
    }

    echo json_encode($ret);
    

    wp_die();
}