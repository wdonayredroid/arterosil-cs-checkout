<?php

use ZendeskCSWooCart\ArterosilTool;
use ZendeskCSWooCart\ArterosilConfig;


use ZendeskCSWooCart\Models\User;
use ZendeskCSWooCart\Models\Customer;
use ZendeskCSWooCart\Models\Agent;
use ZendeskCSWooCart\Models\Order;

use ZendeskCSWooCart\Utils;

add_action( 'wp_ajax_getTest', 'ajax_getTest_handler' );
add_action( 'wp_ajax_nopriv_' . 'getTest', 'ajax_getTest_handler' );



function ajax_getTest_handler() {
    global $wpdb;
    
    $userID = isset($_POST['userID'])?$_POST['userID']:null; 
    $test = isset($_POST['test'])?$_POST['test']:null;
    //set header to return json
    header('Content-Type: application/json');


    // $tool = new ArterosilTool(false);
    // $products  = $tool->getProducts(true,$userID);
    // var_dump($products);
    // return null;
    

    /** MAKE ORDER */

    //$tool = new ArterosilTool(['user_id'=>$userID]);
    //$tool->setCustomer($userID);

    if($test === 'order'){
        //$customer = new Customer( ['user_id'=>195] );
        //$agent = new Agent();   
        //$order = new Order(['customer'=>$customer/*, 'agent'=>$agent*/]);
        //echo json_encode($order->create( [] ));
        Utils::_()->log('test log');
        //$txt = "user id date 123";
        //$myfile = file_put_contents('zendeskwc2.log', 'this is a test'.PHP_EOL , FILE_APPEND | LOCK_EX);
    }

    else if($test==="updateMeta"){
        //echo $userID;
        try{
            $user = new User(['user_id'=>$userID]);
            echo json_encode($user->updateMetaData( ['key'=>'_stripe_customer_id', 'value'=>'12341'] ));
        } catch(Exception $e){
            wp_send_json_error($e->getMessage(), 400);
        }

    }

    else if($test==="cards"){
        $tool->getAllCards();
    }
    else if($test==="createsource"){
        $res = $tool->createSource();
        var_dump($res);
    }
    //WOOCOMMERCE
    else if($test==="getProducts"){
        $res = $tool->getProducts();
        echo json_encode($res);
    }
    else if($test==="createOrder"){

    }
    else if($test==="user"){
        echo $tool->test();
    }
    else if($test==="createToken"){
        //$tool->createPaymentToken();
        $user = new Customer( ['user_id'=>195] );
        //$user = new Agent( ['user_id'=>195] );
        echo json_encode($user->getData());
    }
    else if($test="config"){
        $wc = WC_Payment_Tokens::get_customer_tokens('191');
        echo json_encode($wc);
        foreach($wc as $key => $value){
            // $wc = WC_Payment_Tokens::get(1090);
            //echo json_encode($wc->get_customer_tokens('195'));
            $wc = new WC_Payment_Token_CC($key);
            echo json_encode($wc->get_token());
            echo json_encode($wc->get_meta_data());
        }
        
        
        // echo ArterosilConfig::instance()->getConfig('WOO_CONSUMER_KEY').PHP_EOL;
        // echo ArterosilConfig::instance()->getConfig('WOO_CONSUMER_SECRET').PHP_EOL;
        // echo ArterosilConfig::instance()->getConfig('STRIPE_PUBLISHABLE_KEY').PHP_EOL;
        // echo ArterosilConfig::instance()->getConfig('STRIPE_SECRET_KEY').PHP_EOL;
        // echo ArterosilConfig::instance()->getConfig('STRIPE_CUSTOMER_DESCRIPTION').PHP_EOL;
        // echo ArterosilConfig::instance()->getConfig('WOO_VERSION').PHP_EOL;
        // echo ArterosilConfig::instance()->getConfig('WOO_REST_SOURCE').PHP_EOL;
        // echo ArterosilConfig::instance()->getConfig('WOO_HOST_URL').PHP_EOL;
        // echo ArterosilConfig::instance()->getConfig('CUSTOMER_KEY_REFERENCE').PHP_EOL;
        //var_dump(ArterosilConfig::instance()->keys); 
    }
    else if($test==="getTokens"){ 
        //$wc = WC_Payment_Token::get_customer_tokens('195');
        //echo json_encode($wc);
        //echo 'tokens';
    }

    exit();



    $returned = $tool->createOrder([
        'billing' => [
            'first_name' => 'test name',
            'last_name'  => 'test last name',
            'company'    => '',
            'address_1'  => '',
            'address_2'  => '',
            'city'       => '',
            'state'      => '',
            'postcode'   => '',
            'country'    => '',
            'email'      => 'test@test.com',
            'phone'      => '123451234',
        ],
        'shipping' => [
            'first_name' => 'test shippers name',
            'last_name'  => 'test shippers last name', 
            'company'    => '',
            'address_1'  => 'main st., great city',
            'address_2'  => 'address line 2 here, ',
            'city'       => '',
            'state'      => '',
            'postcode'   => '5123',
            'country'    => 'best country',
            'email'      => 'test@test.com',
            'phone'      => '123451234',    
        ],
        'line_items' => [
            [
                'product_id' => 222,
                'quantity' => 2
            ],
            [
                'product_id' => 170,
                'quantity' => 1
            ]
        ]
    ]);

    if($returned){
        //process payment
        $res = $tool->processPayment('card_1F1sa6AKZbexXQ0j3dXdZqjF',$returned->get_id());
        var_dump($res);
    }

    exit();
}