//TEMPORARY GLOBAL DECLARATION FOR DEBUGGING//
var ORDERFORM;
var temprenderCartView;
var APPDATA;
//////////////////////////////////////////////
(function( $ ) {
	'use strict';

    /**
     * arterosil_ajaxurl = holds ajax url
     */
    var GLOBALS = {
        ajaxurl:arterosil_ajaxurl,
    };
    temprenderCartView = renderCartView;
    ORDERFORM = {
            cart: {},
            productList: null,
            templates: {},
            order: {}
        };
    APPDATA = {};

    ///////////////////////////////////////////////////////
    ///////////////////////MAIN////////////////////////////
    ///////////////////////////////////////////////////////

    $(document).ready(function(){
        initData()
        .then(function(){
            
            orderSnapshotEngine().init();

            initTemplate();
        
            initProductSelect2();
            initQuantityChangeHandler();

            renderView().billing();
            renderView().shipping();

            initAddProductButton();

            //init main order process
            initProcessOrderButton();


            //stripe js
            //ZendeskWooCard_Stripe().init();
            spinner().done();
            
        })
        .fail(function(e){
            spinner().failed(e.responseJSON.data,false);    
        });
        
    });
    ///////////////////////////////////////////////////////
    ///////////////////////END : MAIN//////////////////////
    ///////////////////////////////////////////////////////


    /** =============================
     * Order Snapshot Engine
     ============================= */
    function orderSnapshotEngine(){
        
        var STORAGEKEY = 'ZCSWooCart';

        return {
            init: function(){
                if(sessionStorage[STORAGEKEY]){
                    var sessionData = JSON.parse(sessionStorage[STORAGEKEY]);
                    ORDERFORM = sessionData.ORDERFORM;  
                    renderCartView(); 
                }
                else {
                    sessionStorage[STORAGEKEY] = JSON.stringify({
                        ORDERFORM:{
                            cart: ORDERFORM.cart,
                            order: ORDERFORM.order,
                        }
                    });
                }

            },
            update: function(){
                sessionStorage.setItem(STORAGEKEY,JSON.stringify({
                    ORDERFORM:{
                        cart: ORDERFORM.cart,
                        order: ORDERFORM.order,
                    }
                }));
            },
            clear: function(){
                sessionStorage[STORAGEKEY] = '';
            }
        };
    }

    /**
     * Initialize Process Order Button event
    */
    function initProcessOrderButton(){
        $('[data-aaction="processOrder"]').on('click',(e)=>{
            e.preventDefault();

            spinner().loading();

            if( $(e.currentTarget).attr('data-addcard')=='true' ){
                var obj = e.currentTarget;
                ZendeskWooCard_Stripe().submit(function(d){
                    var payload = d.data;
                    
                    //update card list
                    APPDATA.cards[ payload.paymentTokenId ] = payload[ payload.paymentTokenId ];
                    renderView().cards();

                    processOrder( payload[ payload.paymentTokenId ] )
                    .then(function(res){
                        orderSnapshotEngine().clear();
                        $(obj).attr('data-addcard','');
                        spinner().completed('Successfully completed purchase!');
                    });
                    
                });
            }
            else {
                processOrder().then(function(res){
                    orderSnapshotEngine().clear();
                    spinner().completed('Successfully completed purchase!');   
                });
            }
            
        });
    }

    /**
     * Load Dependencies and Data
     */
    function initData(){
        var dfd = new $.Deferred();
        
        //load user data
        if(USERID){
            var args = {
                userID:USERID,
                action:'getAll',
                nonce:''
            };
            $.post(arterosil_ajaxurl,args)
            .done(function(data){
                APPDATA['customer'] = data.model.customer;
                APPDATA['user'] = data.model.customer;
                APPDATA['products'] = data.model.products;
                APPDATA['cards'] = data.model.tokens;
                APPDATA['config'] = data.model.config;
                //render select2 option box
                updateSelect2Options();

                renderView().customer();
                dfd.resolve(true);
            })
            .fail(function(data){
                dfd.reject(data);
            });
        }

        return dfd.promise();
    }

    /**
     * Init Form Update Button
     */
    function initFormUpdateButton(){
        $('[data-aaction="formUpdate"]').off('click');
        $('[data-aaction="formUpdate"]').on('click',function(e){
            e.preventDefault();
            $(this).closest('.form-update').find('.form-update__view').hide();
            $(this).closest('.form-update').find('.form-update__edit').fadeIn();
        });
    }

    /**
     * Initializes Add Product Button generated by the "Order Form" endpoint.
     */
    function initAddProductButton(){
        $('[data-aaction="addProduct"]').on('click',function(e){
            e.preventDefault();
            $(this).hide();
            $(this).closest('.product-list').find('.add-product-wrapper').removeClass('a-hidden');
            
            //alert( $(this).closest('.add-product-wrapper'));

        });

        //attach event to add to cart button
        $('.btn[data-aaction="addToCart"]').on('click',function(e){
            e.preventDefault();
            $(this).closest('.add-product-wrapper')
            .addClass('a-hidden')
            .prev().show();

            //inject product items
            if(ORDERFORM.productList[0].$$selected){
                ORDERFORM.cart[ORDERFORM.productList[0].$$selected.value] = {
                    text:       ORDERFORM.productList[0].$$selected.text,
                    quantity:  ORDERFORM.productList[0].$$selected.quantity,
                    productObj: APPDATA.products[ '_'+ORDERFORM.productList[0].$$selected.value+'_' ],
                    discount: ORDERFORM.productList[0].$$selected.discount,
                    savings: ORDERFORM.productList[0].$$selected.savings,
                };
                renderCartView();
            }
            orderSnapshotEngine().update();

        });
    }

    /**
     * INIT TEMPLATES
    */
    function initTemplate(){
        var viewTemplates = {};

        viewTemplates['addCard'] = Handlebars.compile( document.getElementById("tplAddCard").innerHTML );
        viewTemplates['billing'] = Handlebars.compile( document.getElementById("tplBilling").innerHTML );
        viewTemplates['shipping'] = Handlebars.compile( document.getElementById("tplShipping").innerHTML );

        ORDERFORM.templates = viewTemplates;
    }


    /**
     * Rend Cart View
     */
    function renderCartView(){

        if(!$.isEmptyObject(ORDERFORM.cart)){
            
            var ret = "<tr> <th>ID</th> <th>Item</th> <th>Qty</th> <th>Savings</th> <th></th> </tr>";
            for(var key in ORDERFORM.cart){
                var discountText = ORDERFORM.cart[key].discount+ "% OFF - $" +(ORDERFORM.cart[key].savings*ORDERFORM.cart[key].quantity).toFixed(2);
                if(parseInt(ORDERFORM.cart[key].discount) < 1){
                    discountText = "N/A";
                }
                ret += '<tr id="'+key+'"> <td>'+key+'</td>  <td>'+ORDERFORM.cart[key].text+'</td>  <td>'+ORDERFORM.cart[key].quantity+'</td> <td>'+discountText+'</td> <td><a href="#" class="remove">REMOVE</a></td>:</tr>';
            }

            // REMOVE button is clicked on the cart
            var table = $(ret);
            table.find('.remove').on('click',function(e){
                e.preventDefault();
                var id = $(this).closest('tr').attr('id');
                delete ORDERFORM.cart[id];
                
                renderCartView();

                orderSnapshotEngine().update();
            });

            $('.product-list').removeClass('no-items')
            .find('table.product-list-table').html("").append(table);
            
        }
        else {
            $('.product-list').addClass('no-items')
            .find('table.product-list-table').html("");    
        }
        renderView().orderSummary();
    }

    /**
     * Init Product Select 2 Dropdown
     */
    function initProductSelect2(){
        //initialize select2 for products
        ORDERFORM
        .productList = $('.wc-product-list')
        .select2({ placeholder: 'Select Product'})                //init
        .on('change', function(e){  //bind change event
            var selectedValue = e.currentTarget.options[e.currentTarget.options.selectedIndex].getAttribute('value');
            selectedValue = '_'+selectedValue+'_';
            var regularPrice = APPDATA.products[selectedValue].regular_price;
            var rolePrice = APPDATA.products[selectedValue].meta_data.festiUserRolePrices[APPDATA.customer.role];

            if(!rolePrice) rolePrice = APPDATA.products[selectedValue].regular_price;

            e.currentTarget['$$selected'] = {
                quantity: $(e.currentTarget).closest('.add-product-wrapper').find('input[name="product-quantity"]').val(),
                text: e.currentTarget.options[e.currentTarget.options.selectedIndex].text,
                value: Utils.replaceAll('_','',selectedValue),
                discount: (((regularPrice - rolePrice)/regularPrice) * 100).toFixed(2),
                savings: (regularPrice - rolePrice).toFixed(2)
            };
            console.log(APPDATA.products[selectedValue].regular_price);
            renderView().orderSummary();
        });
    }

    /**
     * Init On Change Handler For Quantity field
     */
    function initQuantityChangeHandler(){
        //Bind change event for Quantity field
        ORDERFORM
        .productList
        .closest('.add-product-wrapper').find('input[name="product-quantity"]')
        .on('change', function(e){
            if(typeof ORDERFORM.productList[0]['$$selected'] === 'undefined'){
                ORDERFORM.productList[0]['$$selected'] = {};  
            }
            ORDERFORM.productList[0].$$selected['quantity'] = e.currentTarget.value;

            renderView().orderSummary();
        });
    }

    /**
     * Update Select2Options
    */
    function updateSelect2Options(){
        if(APPDATA.products){
            var products = APPDATA.products;
            var role = APPDATA.customer.role;
            $('.wc-product-list').append('<option></option>');
            for(var key in products){
                var $price = products[key].meta_data.festiUserRolePrices[role];
                if(!$price){
                    $price = products[key].regular_price;
                }
                var $option =  '<option value="'+ products[key].id +'">$'+ $price +" - "+products[key].name+'</option>';
                $('.wc-product-list').append($($option));
            }
        }

        renderView().cards();
        
    }

    /** ====================================
     * View Render Engine
    *** ==================================*/
    function renderView(){
        
        console.log('APPDATA: ',APPDATA);
        return {
            customer: function(){
                var $customer = APPDATA.customer;
                $('.customer__name').find('span').text($customer.display_name);
                $('.customer__role').find('span').text($customer.role);
            },
            orderSummary: function(){
                var $total = 0;
                console.log('ORDER >> ', ORDERFORM.cart);
                for(var key in ORDERFORM.cart){
                    var price = ORDERFORM.cart[key].productObj.meta_data.festiUserRolePrices[ APPDATA.customer.role ];
                    if(!price) price = ORDERFORM.cart[key].productObj.regular_price;
                    $total += parseFloat(price) * parseFloat(ORDERFORM.cart[key].quantity ) 
                }
                $('.summary__order-total').find('span').text('$'+$total.toFixed(2));
            },
            billing: function(){
                $('.billing-outer').html( $(ORDERFORM.templates.billing( { data:APPDATA.customer.billing, config:APPDATA.config} )) );
                
                //update click event
                $('.billing-outer').find('[data-aaction="updateBilling"]').on('click', function(e){
                    e.preventDefault();

                    var parentForm = $(this).closest('.form-update__edit');
                    var orderData = {
                        userID: USERID,
                        action: 'updateBilling'
                    };   

                    parentForm.find('[name="billing_address_1"]').val()     && (orderData['billing_address_1']  = parentForm.find('[name="billing_address_1"]').val());
                    parentForm.find('[name="billing_address_2"]').val()     && (orderData['billing_address_2']  = parentForm.find('[name="billing_address_2"]').val());
                    parentForm.find('[name="billing_city"]').val()          && (orderData['billing_city']       = parentForm.find('[name="billing_city"]').val());
                    parentForm.find('[name="billing_company"]').val()       && (orderData['billing_company']    = parentForm.find('[name="billing_company"]').val());
                    parentForm.find('[name="billing_country"]').val()       && (orderData['billing_country']    = parentForm.find('[name="billing_country"]').val());
                    parentForm.find('[name="billing_first_name"]').val()    && (orderData['billing_first_name'] = parentForm.find('[name="billing_first_name"]').val());
                    parentForm.find('[name="billing_last_name"]').val()     && (orderData['billing_last_name']  = parentForm.find('[name="billing_last_name"]').val());
                    parentForm.find('[name="billing_state"]').val()         && (orderData['billing_state']      = parentForm.find('[name="billing_state"]').val());
                    parentForm.find('[name="billing_postcode"]').val()      && (orderData['billing_postcode']   = parentForm.find('[name="billing_postcode"]').val());

                    $.post(arterosil_ajaxurl,orderData)
                    .done(function(data){
                        
                        var keys = Object.keys(data);

                        for(const index in keys){
                            if(data[ keys[index] ].success == true){
                                console.log("keys[index] >> ", keys[index]);
                                APPDATA.customer.billing[ keys[index] ] =   data[ keys[index] ].value;      
                            }
                        }
                        renderView().billing();
                        spinner().done();
                    })
                    .fail(function(data){
                        spinner().failed(data.responseJSON.data.message);
                        console.error("Error while saving Billing Data", data);
                    });
                    
                    $(this).closest('.form-update').find('.form-update__edit').hide();
                    $(this).closest('.form-update').find('.form-update__view').fadeIn();


                });

                //init click events
                initFormUpdateButton();
            },
            shipping: function(){
                $('.shipping-outer').html( $(ORDERFORM.templates.shipping( { data:APPDATA.customer.shipping, config:APPDATA.config } )) );
                $('.shipping-outer').find('[data-aaction="updateShipping"]').on('click', function(e){
                    e.preventDefault();

                    var parentForm = $(this).closest('.form-update__edit');
                    var orderData = {
                        userID: USERID,
                        action: 'updateShipping'
                    };   

                    parentForm.find('[name="shipping_address_1"]').val()     && (orderData['shipping_address_1']  = parentForm.find('[name="shipping_address_1"]').val());
                    parentForm.find('[name="shipping_address_2"]').val()     && (orderData['shipping_address_2']  = parentForm.find('[name="shipping_address_2"]').val());
                    parentForm.find('[name="shipping_city"]').val()          && (orderData['shipping_city']       = parentForm.find('[name="shipping_city"]').val());
                    parentForm.find('[name="shipping_company"]').val()       && (orderData['shipping_company']    = parentForm.find('[name="shipping_company"]').val());
                    parentForm.find('[name="shipping_country"]').val()       && (orderData['shipping_country']    = parentForm.find('[name="shipping_country"]').val());
                    parentForm.find('[name="shipping_first_name"]').val()    && (orderData['shipping_first_name'] = parentForm.find('[name="shipping_first_name"]').val());
                    parentForm.find('[name="shipping_last_name"]').val()     && (orderData['shipping_last_name']  = parentForm.find('[name="shipping_last_name"]').val());
                    parentForm.find('[name="shipping_state"]').val()         && (orderData['shipping_state']      = parentForm.find('[name="shipping_state"]').val());
                    parentForm.find('[name="shipping_postcode"]').val()      && (orderData['shipping_postcode']   = parentForm.find('[name="shipping_postcode"]').val());

                    $.post(arterosil_ajaxurl,orderData)
                    .done(function(data){
                        var keys = Object.keys(data);
                        for(const index in keys){
                            if(data[ keys[index] ].success == true){
                                APPDATA.customer.shipping[ keys[index] ] =   data[ keys[index] ].value;      
                            }
                        }
                        renderView().shipping();
                        spinner().done();
                    })
                    .fail(function(data){
                        spinner().failed(data.responseJSON.data.message);
                        console.error("Error while Saving new shipping data", data);
                    });
                    
                    $(this).closest('.form-update').find('.form-update__edit').hide();
                    $(this).closest('.form-update').find('.form-update__view').fadeIn();
                    
                });

                //init click event
                initFormUpdateButton();
            },
            cards: function (preSelect){
                var selected = "";
                if(preSelect){
                    selected = preSelect;
                }
                if(APPDATA.cards){
                    var cards = APPDATA.cards;
                    $('.stripe-cards').html('');
                    $('.stripe-cards').append('<option value="clear">-- Select payment method --</option>');
                    for(var key in cards){
                        var $option =  '<option '+(cards[key].token===selected?'selected':'')+' value="'+ key +'">Card Ending: '+ cards[key].last4+" - Expiry: "+cards[key].expiry_month+"/"+cards[key].expiry_year+'</option>';
                        $('.stripe-cards').append($($option));
                    }    
                    $('.stripe-cards').append($('<option value="usenew" >Use a new card</option>'));
                }
                $('.add-card-outer').html('');
                $('.stripe-cards').parent().show().next().show();
        
                //add event on change event
                $('.stripe-cards').on('change',function(e){
        
                    $('.add-card-outer').html(''); //clear cards UI
                    $('[data-aaction="processOrder"]').attr('data-addcard','');
        
                    if($(this).children('option:selected').val() === 'usenew'){
        
                        $('.add-card-outer')
                        .append($( ORDERFORM.templates.addCard({}) ));
                        ZendeskWooCard_Stripe().init();
                        $('[data-aaction="processOrder"]').attr('data-addcard',true);
                        
                    }
                    else {
        
                    }
        
                });
                
        
            }
        };
    }

    /**
     * PROCESS ORDER
    */
   function processOrder(cardData = null){
        console.log('PROCSSING ORDER ... ');
        var dfd = new $.Deferred();
        var line_items = [];

        spinner().loading();

        for(var key in ORDERFORM.cart){
            line_items.push({
                'product_id' : key,
                'quantity' : ORDERFORM.cart[key].quantity
            });
        }
        var orderData = {
            userID: USERID,
            card: (cardData?cardData: APPDATA.cards[ $($('.stripe-cards')[0][ $('.stripe-cards')[0].selectedIndex ]).val() ] ),
            line_items: line_items,
            action: 'processOrder'
        };    
        console.log('ORDER DATA >> ', orderData);

        $.post(arterosil_ajaxurl,orderData)
        .done(function(data){
            console.log('DONE PROCESSING ORDER >> ', data);
            spinner().completed('Successfully completed purchase!');
            dfd.resolve(data);
        })
        .fail(function(data){
            spinner().failed(data.responseJSON.data.message);
            console.error("Error while fetching User Data", data);
            dfd.reject(data);
        });

        return dfd.promise();
   }

   /**
    * SPINNER
   */
   function spinner(){
       var orderForm = $('.order-form');
       var loader = $('.order-form .circle-loader-outer');

       var status = 0;

       return {
           /*=============================*/
           loading: function(){
            if(status!=1){
                status = 1;
                orderForm
                .addClass('loading')
                .find('.circle-loader')
                .removeClass('load-complete')
                .removeClass('load-failed');
                loader.focus();
            }
           },
           /*=============================*/
           done: function(){
            status = 0;
            orderForm
            .removeClass('loading')
            .find('.circle-loader')
            .removeClass('load-complete')
            .removeClass('load-failed');

            orderForm.find('.order-form-text').text('');
           },
           /*=============================*/
           failed: function(message,destroy=true){
            if(status!=2){
                status = 2;
                orderForm
                .addClass('loading')
                .find('.circle-loader')
                .addClass('load-failed');

                orderForm.find('.order-form-text').text(message);
                
                loader
                .focus()
                .off('click')
                .on('click',function(){
                    if(destroy){
                        status = 0;
                        orderForm
                        .removeClass('loading')
                        .find('.circle-loader')
                        .removeClass('load-complete');
                        orderForm.find('.order-form-text').text('');
                    }
                });
            }
           },
           /*=============================*/
           completed: function(message){
            if(status!=3){
                status = 3;
                orderForm
                .addClass('loading')
                .find('.circle-loader')
                .addClass('load-complete');

                orderForm.find('.order-form-text').text(message);
                
                loader
                .focus()
                .off('click')
                .on('click',function(){
                    status = 0;
                    orderForm
                    .removeClass('loading')
                    .find('.circle-loader')
                    .removeClass('load-complete');
                    orderForm.find('.order-form-text').text('');
                    location.reload();
                });
            }
           },

       };
   }
    
   //================================================================
   /**
    * HANDLEBAR HELPERS
    */
   //================================================================
    window.Handlebars.registerHelper('select', function( obj, value, options){

        var isObjArray = false;
        if(Array.isArray(value)){
            value = value[0];
        }

        if(Array.isArray(obj)){
            isObjArray = true;
        }

        var out = "";

        var keys = Object.keys(obj);

        for(const index in keys){
            if(!isObjArray){
                out = out + "<option "+ (keys[index]===value?'selected':'') +" value=\""+keys[index]+"\">" + obj[ keys[index] ].name + "</option>";
            }
            else {
                out = out + "<option "+ ( obj[keys[index]].code ===value?'selected':'') +" value=\""+obj[keys[index]].code+"\">" + obj[ keys[index] ].name + "</option>";       
            }
        }

        return out;
    });

    Handlebars.registerHelper('ifEquals', function(arg1, arg2, options) {
        
        if(Array.isArray(arg1)) arg1 = arg1[0];
        
        return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
    });


    /**
     * Utility Functions
    */
    var Utils = {
        replaceAll: (search, replacement, target) => {
            return target.replace(new RegExp(search, 'g'), replacement); 
        }
    }

    /**
     * STRIPE Script
    */
    var card = null;
    var elements;
    var ZendeskWooCard_Stripe = function(){


        function init(){
            card = null;
            elements = stripe.elements();
            var style = {
                base: {
                    // Add your base input styles here. For example:
                    fontSize: "16px",
                    color: "#32325d",
                    width: "100% !important"
                }
            };
              
            // Create an instance of the card Element.
            card = elements.create("card", {style: style});
            console.log('INIT CARD >> ', card);
            
            // Add an instance of the card Element into the `card-element` <div>.
            card.mount("#card-element-2");

            card.addEventListener("change", function(event) {
                var displayError = document.getElementById("card-errors");
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = "";
                }
            });

            var form = document.getElementById("add-new-card");
            
            form.addEventListener("submit", function(event) {
                event.preventDefault();

                submit(); //trigger form submit

            });
        }

        function submit(cbSuccess=null){
            console.log('CARD >> ', card);
            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the customer that there was an error.
                    var errorElement = document.getElementById("card-errors");
                    errorElement.textContent = result.error.message;
                }
                else {
                    //submit
                    if(result.token){
                        var token = result.token;
                        var payload = {
                            'action':'addCard',
                            'token': token.id,
                            'type': token.type,
                            'brand': token.card.brand,
                            'country': token.card.country,
                            'exp_month': token.card.exp_month,
                            'exp_year':token.card.exp_year,
                            'last4':token.card.last4,
                            'card_id': token.card.id,
                            'object': token.card.object,
                            'created':token.created,
                            'userID': USERID
                        };
                        $.post(arterosil_ajaxurl,payload)
                        .done(function(data){
                            if(cbSuccess){
                                cbSuccess(data);
                            }
                        });
                    }

                }
            });
        }
        
        return {
            init: init,
            submit:submit
        };
    }

})( jQuery );
