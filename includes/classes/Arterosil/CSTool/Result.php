<?php 

namespace ZendeskCSWooCart;

trait Result{

    public function result($args){

        Utils::_()->args([
            ['data'],
            ['success', false],
            ['message','']
        ], $args); 

        return $args;
  
    }
}