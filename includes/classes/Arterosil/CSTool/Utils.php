<?php

/**
 * Utility Functions
 *
 * @package  ZendeskCSWooCart\Utils
 */

namespace ZendeskCSWooCart;

class Utils
{

    private static $instance = null;  
    
    private function __construct(){}

    public static function _(){
        if(!self::$instance){
            self::$instance = new Utils();
        }
        return self::$instance;
    }

    /**
     * Make sure arguments are properly formatted with assigned default value
     * @param Array $args e.g. [ ['{propertyName}','{defaultValue}'] ]
    */
    public function args($args,&$src){
        foreach($args as $value){
            if(!isset($value[1])) $value[1] = null;
            $src[ $value[0] ] = isset( $src[$value[0]] ) ? $src[$value[0]] : $value[1];
        }
    }

    /**
     * Error Log
    */
    public function log($message){
        $myfile = file_put_contents('zendeskwc.log', $message.PHP_EOL , FILE_APPEND | LOCK_EX);
    }
}