<?php

namespace ZendeskCSWooCart\Models;

use Automattic\WooCommerce\Client;
use ZendeskCSWooCart\ArterosilConfig as Config;

class ZendeskCS_Woocommerce
{

    private static $instance = null;  

    private $wc; //woocommerce rest api instance

    public $keys = [];
    
    public function __construct(){

        $remoteSource = Config::_()->getConfig( 'WOO_REST_SOURCE' );
        if($remoteSource == true){
            $this->wc = new Client(
                Config::_()->getConfig('WOO_HOST_URL'), 
                Config::_()->getConfig('WOO_CONSUMER_KEY'), 
                Config::_()->getConfig('WOO_CONSUMER_SECRET'),
                [
                    'wp_api' => true,
                    'version' => "wc/v3",
                    'query_string_auth' => true
                ]
            );
        }
    }

    public static function _(){
        if(!self::$instance){
            self::$instance = new ZendeskCS_Woocommerce();
        }
        return self::$instance;
    }

    public function get($path){
        return $this->wc->get($path);
    }

    public function post($path,$data){
        return $this->wc->post($path,$data);
    }

    public function put($path,$data){
        return $this->wc->put($path,$data);
    }


}