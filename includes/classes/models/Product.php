<?php

namespace ZendeskCSWooCart\Models;

class Product
{

    private $data = [];

    public function __construct($data=[]){
        $this->data = $data;

        $this->initializeMetadata();

    }

    /**
     * Process Meta Data into array
    */
    private function initializeMetadata(){

        $meta = $this->data->meta_data;

        $this->data->meta_data = [];

        if(isset($meta)){
            foreach($meta as $value){

                $jsonData = json_decode($value->value);
                if(json_last_error() == JSON_ERROR_NONE){
                    $value->value = $jsonData;
                }

                if(isset( $this->data->meta_data[$value->key] )){
                    if(!is_array($this->data->meta_data[$value->key])){
                        $currentValue = $this->data->meta_data[$value->key];
                        $this->data->meta_data[$value->key] = [];
                        $this->data->meta_data[$value->key][] = $currentValue;
                    }
                    $this->data->meta_data[$value->key][] = $value->value;
                }
                else{
                    $this->data->meta_data[$value->key] = $value->value;
                }
            }
        }
    }

    /**
     * Get All Meta
    */
    public function getAllMeta(){
        return $this->allMeta;
    }

    /**
     * Get Model Data
    */
    public function getData($key=null){
        if(isset($key)){
            return $this->data[$key];    
        }
        else {
            return $this->data;
        }
        
    }
    
}