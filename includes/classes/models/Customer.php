<?php  

namespace ZendeskCSWooCart\Models;

use ZendeskCSWooCart\ArterosilConfig;
use ZendeskCSWooCart\Utils;

class Customer extends User{

    private $stripe;

    private $data = [
        'stripe_id' => ''
    ];


    public function __construct($args){
        Utils::_()->args([
            ['user_id']
        ],$args);
        
        if(!isset($args['user_id'])) throw new \Exception('user id is required!');

        parent::__construct([ 'user_id' => $args['user_id'] ]);

        $parentData = parent::getData();
        $meta = parent::getAllMeta();

        $stripeRef = ArterosilConfig::instance()->getConfig('CUSTOMER_KEY_REFERENCE');
        $this->data['stripe_id'] = isset($meta->$stripeRef) ? reset( $meta->$stripeRef ) : null ; 
        $this->data = array_merge($parentData,$this->data);

    }


    public function getData($key=null){
        $parentData = parent::getData(null);
        $this->data = array_merge($parentData,$this->data);

        if(isset($key)){
            return $this->data[$key];    
        }
        else {
            return $this->data;
        }

    }


    public function setStripeAPI(&$stripe){
        $this->stripe = $stripe;
        //sync stripe info
        if(!$this->isOnStripe()){
            $this->data['stripe_id'] = null;    
        }
        
    }

    
    public function isOnStripe(){
        
        if(isset($this->data['stripe_id'])){
            $res = ($this->stripe->getCustomerById($this->data['stripe_id']));;
            if( empty($res) || (isset($res->deleted) && $res->deleted) ){
                return false;
            }
            else {
                return true;
            }
        }
        return false;
    }


}