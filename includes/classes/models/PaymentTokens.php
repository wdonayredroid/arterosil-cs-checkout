<?php 

namespace ZendeskCSWooCart\Models;

use ZendeskCSWooCart\ArterosilConfig;

class PaymentTokens{

    private $remoteSource = 1;
    private $config = [];
    private $data = [];
    private $userID = null;


    public function __construct($args = []){

        $args = [
            'user_id'       =>  isset($args['user_id'])         ? $args['user_id']  : null,
            'force_local'   =>  isset($args['force_local'])   ? $args['force_local']  : false
        ];
        
        if(empty($args['user_id'])) throw new \Exception('user id is missing!');

        $this->userID = $args['user_id'];
        $this->config = ArterosilConfig::instance();

        //set remote source by config or local

        if($args['force_local']){
            $this->setRemoteSource($args['force_local']);
        }
        else {
            $this->setRemoteSource(intval($this->config->getConfig( 'WOO_REST_SOURCE' )));
        }
        
        $this->getTokens();
    
    }


    /**
     * Get Payment Tokens
     * @return Object 
    */
    private function getTokens(){
        
        if(!$this->isRemote()){
            var_dump('not remote currently!');
            $this->data = 'not remote';
            // $wc = WC_Payment_Tokens::get_customer_tokens($this->userID);

            // $ret = [];

            // foreach($wc as $key => $value){
            //     $cc = new WC_Payment_Token_CC($key);
            //     // var_dump($cc->get_data());
            //     $meta = $cc->get_meta_data();
            //     $ret[$key] = [
            //         'token'         => $cc->get_token(),
            //         'last4'         => $cc->get_meta('last4'),
            //         'expiry_year'   => $cc->get_meta('expiry_year'),
            //         'expiry_month'  => $cc->get_meta('expiry_month'),
            //         'card_type'     => $cc->get_meta('card_type'),
            //         'gateway_id'    => $cc->get_data()['gateway_id']
            //     ];
            // }
        }
        else{
            $response = wp_remote_post(
                $this->config->getConfig('WOO_HOST_URL').'/wp-admin/admin-ajax.php',
                [
                    'method' => 'POST',
                    'blocking' => true,
                    'body' => [
                        'action' => 'getPaymentTokens',
                        'userID' => $this->userID
                    ],
                    'headers' => []
                ]
            );
            $ret = json_decode($response['body']);
        }
        $this->data = $ret;
    }


    /**
     * Set Remote Flag to false
     * @param bool flag
    */
    public function setRemoteSource($flag){
        $this->remoteSource = $flag;
    }


    /**
     * Get Remote Flag statis
     * @return bool
    */
    private function isRemote(){
        return $this->remoteSource;
    }

    /**
     * Get Data
     * @return Object
    */
    public function getData(){
        $this->getTokens();
        return $this->data;
    }

}