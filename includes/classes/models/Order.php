<?php

namespace ZendeskCSWooCart\Models;

use ZendeskCSWooCart\Models\ZendeskCS_Woocommerce as WC;
use ZendeskCSWooCart\ArterosilConfig as Config;
use ZendeskCSWooCart\Utils;
use ZendeskCSWooCart\Result;
use \Exception;

class Order
{
    use Result;

    private $remoteSource = 0;
    private $config = null;

    private $allMeta = [];

    private $customer;
    private $agent;

    private $data = [];

    public function __construct($args){

        Utils::_()->args([
            ['force_local'],
            ['customer',null],
            ['agent',null]
        ],$args);


        if(!isset($args['customer'])) throw new Exception('Customer Object is required!');

        $this->customer = $args['customer'];
        $this->data['customer'] = $args['customer']->getData();

        //$this->agent = $args['agent];
        //$this->data['agent'] = $args['agent']->getData();

        $this->config = Config::_();

        //check remote or local
        if(!isset($args['force_local'])){
            $this->setRemoteSource(intval($this->config->getConfig( 'WOO_REST_SOURCE' ))); //set remote source by config/settings
        }
        else{
            $this->setRemoteSource( !intval( $args['force_local'] ) );    
        } // ~end
    }

    /**
     * Create Order
    */
    public function create($args){
 
        Utils::_()->args([
            ['billing',  $this->customer->getData('billing')],
            ['shipping', $this->customer->getData('shipping') ],
            ['line_items', null]
        ],$args);   

        
        if(!isset($args['line_items'])){
            throw new Exception('line_items required!');
        }
        if($this->remoteSource){
            $this->updateLineItemsPrices($args['line_items']);
            $this->data  = WC::_()->post('orders',$args);
            $this->updateNote([ 
                'note' => 'Order Created and set to pending status.', 
                'added_by_user' => true 
            ]);
        }
        else{
            
        }
        
    }

    /**
     * Update Order Note
    */
    public function updateNote($args){
        return WC::_()->post('orders/'.$this->data->id.'/notes', $args);   
    }

    /**
     * Update Order Status
    */
    public function updateStatus($status){
        return WC::_()->put('orders/'.$this->data->id, [
            'status' => 'processing'
        ]);
    }

    /**
     * Update Line Items before creating an Order
    */
    private function updateLineItemsPrices(&$lineItems){

        $updatedLineItems = [];

        foreach($lineItems as $lineItem){
            $rolePrice = get_price_by_user_id($lineItem['product_id'], $this->customer->getData('ID'));
            $totalPrice = intval($rolePrice) * intval($lineItem['quantity']);

            if(floatval($rolePrice)>0){
                $lineItem['total'] = $totalPrice."";
            }

            $updatedLineItems[] = $lineItem;  
        }
        $lineItems = $updatedLineItems;
    }

    /**
     * Get Order
    */
    public function getOrder($args){
        if(!isset($args['id'])){
            throw new Exception('order id is missing.');
        }  
        
        if($this->remoteSource){
            return WC::_()->get('orders/'.$args['id']);
        }
        else {
            return (wc_get_order($args['id']))->get_data();
        }
    }

    /**
     * Get All Meta
    */
    public function getAllMeta(){
        return $this->allMeta;
    }

    /**
     * Set Remote Flag to false
     * @param bool flag
    */
    public function setRemoteSource($flag){
        $this->remoteSource = $flag;
    }


    /**
     * Get Model Data
    */
    public function getData($key=null){

        if(isset($key)){
            return $this->data[$key];    
        }
        else {
            return $this->data;
        }
        
    }

}