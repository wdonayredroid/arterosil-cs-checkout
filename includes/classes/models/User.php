<?php

namespace ZendeskCSWooCart\Models;

use ZendeskCSWooCart\ArterosilConfig;
use ZendeskCSWooCart\Utils;
use ZendeskCSWooCart\Result;
use \Exception;

class User
{
    use Result;

    private $remoteSource = 0;
    private $config = null;

    private $allMeta = [];

    private $data = [
        'ID' => '',
        'user_login' => '',
        'roles' => [],
        'name' => '',
        'first_name' => '',
        'last_name' => '',
        'user_email' => '',
        'billing' => [],
        'shipping' => []
    ];

    private $userData = [];

    public function __construct($args){

        Utils::_()->args([
            ['user_id',null],
            ['force_local']
        ],$args);

        $userID = $args['user_id'];

        if(empty($userID)) throw new Exception('user id is required!');

        $this->data['ID'] = $userID;

        $this->config = ArterosilConfig::instance();
        $keys = $this->config->keys;

        if(!isset($args['force_local'])){
            $this->setRemoteSource(intval($this->config->getConfig( 'WOO_REST_SOURCE' ))); //set remote source by config/settings
        }
        else{
            $this->setRemoteSource( !intval( $args['force_local'] ) );    
        }

        if(!$this->remoteSource){
            $userData = get_userdata($userID);

            unset($userData->data->user_pass);
            unset($userData->data->user_activation_key);
            
            if($userData){
                $userData = (array)json_decode( json_encode($userData) );
                $meta = get_user_meta($userID);
                $userData['meta'] = $meta;
                $this->mapUserData( $userData );
            }
            else {
                throw new Exception('user id does not exist');
            }
            
        }
        else{
            $response = wp_remote_post(
                $this->config->getConfig('WOO_HOST_URL').'/wp-admin/admin-ajax.php',
                [
                    'method' => 'POST',
                    'blocking' => true,
                    'body' => [
                        'action' => 'getUser',
                        'userID' => $this->data['ID']
                    ],
                    'headers' => []
                ]
            );
            if($response['body']){
                $this->mapUserData( json_decode($response['body']) );
            }
            else {
                throw new Exception('user id does not exist');
            }
            
        }
    }

    /**
     * Get All Meta
    */
    public function getAllMeta(){
        return $this->allMeta;
    }

    /**
     * Set Remote Flag to false
     * @param bool flag
    */
    public function setRemoteSource($flag){
        $this->remoteSource = $flag;
    }

    /**
     * Map User Data from source to this model properties
    */
    private function mapUserData($data){
        $data = json_decode( json_encode($data) );
        foreach($data as $key=>$value){
            if($key === 'data'){
                $this->data['user_login'] = $value->user_login;
                $this->data['user_email'] = $value->user_email;
                $this->data['display_name'] = $value->display_name;
            }
            else if($key === 'roles'){
                $this->data['roles'] = $value;
                $this->data['role'] = reset($value);
            }
            else if($key === 'meta'){
                
                //have a copy of meta
                $this->allMeta = $value;

                $this->data['first_name']       = isset($value->first_name)         ? reset($value->first_name)        : '' ;
                $this->data['last_name']        = isset($value->last_name)          ? reset($value->last_name)         : '' ;
                //$this->data['role']             = isset($value->my_default_role)    ? reset($value->my_default_role)   : '';

                $stripeRef = $this->config->getConfig('CUSTOMER_KEY_REFERENCE');
                //$this->data['stripe_id']        = isset($value->stripeRef) ? reset( $value->$stripeRef ) : '' ;
                
                //billing
                $this->data['billing']['email']         = isset($value->billing_email)      ? reset( $value->billing_email )        : '' ;
                $this->data['billing']['first_name']    = isset($value->billing_first_name) ? reset( $value->billing_first_name )   : '' ;
                $this->data['billing']['last_name']     = isset($value->billing_last_name)  ? reset( $value->billing_last_name )    : '' ;
                $this->data['billing']['company']       = isset($value->billing_company)    ? reset( $value->billing_company )      : '' ;
                $this->data['billing']['address_1']     = isset($value->billing_address_1)  ? reset( $value->billing_address_1 )    : '' ;
                $this->data['billing']['address_2']     = isset($value->billing_address_2)  ? reset( $value->billing_address_2 )    : '' ;
                $this->data['billing']['city']          = isset($value->billing_city)       ? reset( $value->billing_city )         : '' ;
                $this->data['billing']['postcode']      = isset($value->billing_postcode)   ? reset( $value->billing_postcode )     : '' ;
                $this->data['billing']['country']       = isset($value->billing_country)    ? reset( $value->billing_country )      : '' ;
                $this->data['billing']['state']         = isset($value->billing_state)      ? reset( $value->billing_state )        : '' ;
                $this->data['billing']['phone']         = isset($value->billing_phone)      ? reset( $value->billing_phone )        : '' ;

                //shipping
                $this->data['shipping']['first_name']  = isset($value->shipping_first_name)    ? reset( $value->shipping_first_name )  : '' ;
                $this->data['shipping']['last_name']   = isset($value->shipping_last_name)     ? reset( $value->shipping_last_name )   : '' ;
                $this->data['shipping']['company']     = isset($value->shipping_company)       ? reset( $value->shipping_company )     : '' ;
                $this->data['shipping']['address_1']   = isset($value->shipping_address_1)     ? reset( $value->shipping_address_1 )   : '' ;
                $this->data['shipping']['address_2']   = isset($value->shipping_address_2)     ? reset( $value->shipping_address_2 )   : '' ;
                $this->data['shipping']['city']        = isset($value->shipping_city)          ? reset( $value->shipping_city )        : '' ;
                $this->data['shipping']['postcode']    = isset($value->shipping_postcode)      ? reset( $value->shipping_postcode )    : '' ;
                $this->data['shipping']['country']     = isset($value->shipping_country)       ? reset( $value->shipping_country )     : '' ;
                $this->data['shipping']['state']       = isset($value->shipping_state)         ? reset( $value->shipping_state )       : '' ; 
                
            }
        }    
    }

    /**
     * Get Model Data
    */
    public function getData($key=null){

        if(isset($key)){
            return $this->data[$key];    
        }
        else {
            return $this->data;
        }
        
    }

    /**
     * Update Meta
    */
    public function updateMetaData($args){

        Utils::_()->args([
            ['key',null],
            ['value',null]
        ],$args);

        $ret = $this->result([]);

        // check meta key query param
        if(!isset($args['key'])){
            throw new Exception('meta key is required');
        }

        if($this->remoteSource) {
            $ret['message'] = 'processing remote';
            $res = wp_remote_post(
                $this->config->getConfig('WOO_HOST_URL').'/wp-admin/admin-ajax.php',
                [
                    'method' => 'POST',
                    'blocking' => true,
                    'body' => [
                        'action' => 'updateUserMeta',
                        'userID' => $this->getData('ID'),
                        'key' => $args['key'],
                        'value' => $args['value']
                    ],
                    'headers' => []
                ]
            );   
            $res = json_decode($res['body']);
            if(!$res->success){
                throw new Exception('User meta data update failed! Please make sure that the new meta value is different from the current one.');
            }
            else {
                $ret['message'] = 'User meta data update successful';
                $ret['success'] = true;
                return $ret;
            }
        }
        else {
            $res = update_user_meta($this->getData('ID'), $args['key'], $args['value']);
            if(!$res){
                throw new Exception('User meta data update failed! Please make sure that the new meta value is different from the current one.');
            }
            else {
                $ret['message'] = 'User meta data update successful';
                $ret['success'] = true;
                return $ret;
            }
            
        }

    }


    
}